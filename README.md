# Abstract

This is an attempt to document the task warrior tools I'm using, and 
possibly those that I am experimenting with.  My ultimate goal is to 
use task warrior as the central source for many todo engines, as I feel 
it makes it easier to test them out.  And some sort of collaboration 
tool (like taskarena).

# The Tools (currently using):

- task warrior: https://taskwarrior.org
- inthe.am
    - trello sync
    - google cal sync
    - email to add task
- task warrior android app
- task whisperer
    - gnome extension provides nice at a glance view 
- taskopen


# On Deck:

- bugwarrior
- taskarena
- taskwiki
- tasksync
- vim-task
